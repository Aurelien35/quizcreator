package com.MainPackage.Repository;

import QuestionTypes.Question;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class QuestionRepository {
    private HashMap<Integer, Question> questionMap = new HashMap<Integer, Question>();

    public void save(Question question) {
        questionMap.put(question.getQuestionID(), question);
        System.out.println("Question : " + question.getQuestionID() + " Saved");
    }

    public Question getQuestion(int id) {
        return questionMap.get(id);
    }

    public List<Question> getAllQuestions() {
        List<Question> list = new ArrayList<Question>();
        questionMap.forEach((key,value) -> list.add(value));
        return list;
    }

    public void deleteQuestion(int id) {
        questionMap.remove(id);
    }

    public void deleteAllQuestions() {
        questionMap.clear();
    }
}
