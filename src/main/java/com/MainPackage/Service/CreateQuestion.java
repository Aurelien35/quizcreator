package com.MainPackage.Service;

import QuestionTypes.Question;
import com.MainPackage.Factory.CreateNewQuestionFromTmdbApi;
import com.MainPackage.Repository.QuestionRepository;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class CreateQuestion {
    private String questionText;
    private String answerText;
    private int questionID;
    private CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi;
    private TmdbClient tmdbClient;
    private QuestionRepository questionRepository;

    public CreateQuestion(CreateNewQuestionFromTmdbApi createNewQuestion, TmdbClient tmdb,QuestionRepository questionRepo) {
        createNewQuestionFromTmdbApi = createNewQuestion;
        tmdbClient = tmdb;
        questionRepository = questionRepo;
    }

    public int createNewQuestion(String movieName) {
        if(movieName.equals("")){
            createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(new Random().nextInt(3), tmdbClient).ifPresent(questionq -> {
                questionID = questionq.getQuestionID();
                questionRepository.save(questionq);
            });
            return questionID;
        }
        else{
            createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(movieName, new Random().nextInt(3), tmdbClient).ifPresent(questionq -> {
                questionID = questionq.getQuestionID();
                questionRepository.save(questionq);
            });
            return questionID;
        }
    }
    public String getAnswerText() {
        return answerText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public int getQuestionID() {
        return questionID;
    }
}
