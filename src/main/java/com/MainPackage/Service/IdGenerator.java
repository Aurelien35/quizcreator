package com.MainPackage.Service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class IdGenerator {
    private int generatedQuestionId = 0;

    public int generaterQuestionId() {
        return generatedQuestionId++;
    }
}
