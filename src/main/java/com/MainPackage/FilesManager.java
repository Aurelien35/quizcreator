package com.MainPackage;

import QuestionTypes.Question;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.List;

@Component
public class FilesManager {

   // @PostConstruct
    public void initializeFiles(){
        try {
            File file = new File("liste de questions.txt");
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveQuestionsOnFile(Question question) {
        try (FileWriter fw = new FileWriter("liste de questions.txt", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(question.getQuestionText());
            out.println(question.getAnswerText());
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    public void questionsListUpdater(List<Question> questionList) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("liste de questions.txt"));
            String line = "";
            while (line != null) {
                line = reader.readLine();
                String line2 = reader.readLine();
                if (line != null || line2 != null) {
                    //TODO GERER L'ID ICI
                    questionList.add(new Question(1,line, line2, 1));
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
