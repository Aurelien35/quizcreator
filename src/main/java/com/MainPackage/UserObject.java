package com.MainPackage;

public class UserObject {
    private String userName;
    private int userScore;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public int getUserScore() {
        return userScore;
    }
    public void setUserScore(int userScore) {
        this.userScore = userScore;
    }
    public void increaseUserScore(int points) {
        this.userScore += points;
    }
}
