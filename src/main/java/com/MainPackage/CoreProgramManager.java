package com.MainPackage;

import com.MainPackage.Factory.CreateNewQuestionFromTmdbApi;
import QuestionTypes.Question;
import com.MainPackage.Repository.QuestionRepository;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

@Component
class CoreProgramManager {

    private QuestionsManager questionsManager;
    private CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi;
    private FilesManager filesManager;
    private QuestionRepository questionRepository;

    public CoreProgramManager(QuestionsManager incQuestionsManager, FilesManager incFilesManager, CreateNewQuestionFromTmdbApi createNewQuestion,QuestionRepository questionRepo) {
        questionsManager = incQuestionsManager;
        filesManager = incFilesManager;
        createNewQuestionFromTmdbApi = createNewQuestion;
        questionRepository = questionRepo;
    }

    public void start(Scanner sc, TmdbClient tmdbClient) throws IOException {
        UserObject currentUser = new UserObject();
        List<Question> questionsListToAsk = new ArrayList<Question>();
        boolean wantToQuit = false;

     //   System.out.println("Quel est votre nom ?");
     //   currentUser.setUserName(sc.nextLine());

        for (int i = 0; i < 20; i++) {
         //   createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(new Random().nextInt(3), tmdbClient).ifPresent(question -> questionsListToAsk.add(question));
            createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi(new Random().nextInt(3), tmdbClient).ifPresent(question -> questionRepository.save(question));
        }

      /*//  Store Random Questions
        for (Question question : questionsListToAsk) {
            filesManager.saveQuestionsOnFile(question);
        }*/

        filesManager.questionsListUpdater(questionsListToAsk);
        /*
        questionsListToAsk.add(new QCM("capitale de la france ?", "paris", List.of("jambon", "Tourbilol", "gloupi")));
        questionsListToAsk.add(new QCM("comment s'apelle Milou ?", "Milou", List.of("mileu", "leuleu", "loumi")));
        questionsListToAsk.add(new QCM("Quel est le 7eme nombre de la suite de Fibonachi ?", "13", List.of("7", "2", "11")));
        questionsListToAsk.add(new TrueFalse("Le soleil tourne autour de la terre ?", false));
        questionsListToAsk.add(new TrueFalse("L'effet Streisand correspond au fait de très mal chanter", false));
        questionsListToAsk.add(new TrueFalse("l'abus d'alcool est dangereux pour la santé ", true));
*/

        System.out.println("Bonjour " + currentUser.getUserName() + " , Veuillez répondre à ces questions :");

        while (!wantToQuit) {
            currentUser.setUserScore(0);
            for (Question q : questionsListToAsk) {
                if (questionsManager.AskAllTypesOfQuestions(q)) {
                    currentUser.increaseUserScore(q.getQuestionPoints());
                }
            }

            if (currentUser.getUserScore() > questionsListToAsk.size())
                System.out.println("Bravo " + currentUser.getUserName() + " ! Vous avez remporté : " + currentUser.getUserScore() + " Points");
            else {
                System.out.println("Mouais " + currentUser.getUserName() + " , Vous n'avez remporté que : " + currentUser.getUserScore() + " Points");
            }

            System.out.println("Voulez vous recommencer ? y/n");
            if (sc.nextLine().equals("n")) {
                wantToQuit = true;
            }
        }

        /*
        while (!isReadyToStartGame) { //boucle mise en place pour ajouter plusieurs questions jusqu'a ce que isReadyToStartGame soit True
            questionsListUpdater(); // Je met à jour la liste de questions au cas ou le fichier texte contienne déjà des questions
            System.out.println("Bien le bonjour " + currentUser.getUserName() + "  voulez vous ajouter une question y/n");
            if (sc.nextLine().equals("y")) { // je met le sc.nextline directement dans le If pour Optimiser le code
                saveQuestionsOnFile(addQuestion()); // Je lance la méthode de création de question DANS la méthode de sauvegarde des questions sur le fichier.
            } else {
                if (questionsList.size() == 0) { // si la taille de la liste après avoir lancé questionsListUpdater(); est vide
                    System.out.println("Il n'y à pas encore de question");
                    saveQuestionsOnFile(addQuestion()); // je force la création de question
                } else {
                    isReadyToStartGame = true; // si on ne veux pas faire de question et qu'il y en a déjà, la boucle ne se relance pas
                }
            }
        }
         */
    }
/*
    private Question addQuestion(Scanner sc) {
        System.out.println("Rentrez votre question : ");
        String question = sc.nextLine();
        System.out.println("Rentrez votre réponse : ");
        String answer = sc.nextLine();
        return new Question(question, answer, 1); // je retourne directement un QuestionObject en utilisant son constructeur
    }
 */
}
