package com.MainPackage.Controllers;

import com.MainPackage.Repository.QuestionRepository;

public class QuestionRepresentation {

    private int id;
    private String displayableText;

    public QuestionRepresentation(int idNumber, String displayText) {
        id = idNumber;
        displayableText = displayText;
    }

    public int getId() {
        return id;
    }

    public String getDisplayableText() {
        return displayableText;
    }
}
