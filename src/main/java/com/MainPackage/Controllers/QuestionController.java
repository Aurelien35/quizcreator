package com.MainPackage.Controllers;

import QuestionTypes.Question;
import com.MainPackage.Factory.CreateNewQuestionFromTmdbApi;
import com.MainPackage.Repository.QuestionRepository;
import com.MainPackage.Service.CreateQuestion;
import com.zenika.academy.tmdb.TmdbClient;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
public class QuestionController {
    private QuestionRepository questionRepository;
    private CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi;
    private TmdbClient tmdbClient;
    private  CreateQuestion createQuestion;

    public QuestionController(QuestionRepository questionRepo, CreateNewQuestionFromTmdbApi createNewQuestion, TmdbClient tmdb,CreateQuestion createQ) {
        questionRepository = questionRepo;
        createNewQuestionFromTmdbApi = createNewQuestion;
        tmdbClient = tmdb;
        createQuestion = createQ;
    }

    @GetMapping("/setting")
    public Question createQuestion(@RequestParam(value = "movieName", defaultValue = "") String movieName) {
        return questionRepository.getQuestion(createQuestion.createNewQuestion(movieName));
    }

    @GetMapping("/toto")
    public List<Question> readQuestion(){
        return questionRepository.getAllQuestions();}


    @PostMapping("/crabipouet")
    public Question tryAnswer (@RequestParam(value = "answer", defaultValue = "") String movieName) {
        return questionRepository.getQuestion(createQuestion.createNewQuestion(movieName));
    }


}

