package QuestionTypes;

import org.apache.commons.text.similarity.LevenshteinDistance;

public class Question {
    protected String questionText;
    protected String answerText;
    protected int questionPoints;
    protected int questionID;
    protected LevenshteinDistance lev = new LevenshteinDistance();
    private int levDistanceCurrentlyUsed = 1;

    public Question(int id, String question, String answer, int points) {
        questionText = question;
        questionID = id;
        answerText = answer;
        questionPoints = points;
    }

    public String getQuestionText() {
        return questionText;
    }

    public int getQuestionPoints() {
        return questionPoints;
    }

    public String getAnswerText() {
        return answerText;
    }

    public int getQuestionID() {
        return questionID;
    }

    public boolean checkAnswer(String userAnswer) {
        if (lev.apply(answerText, userAnswer) <= levDistanceCurrentlyUsed) {
            return true;
        } else {
            return false;
        }
    }
}
