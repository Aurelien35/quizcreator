package QuestionTypes;

import java.util.List;

public class TrueFalse extends Question {
    List<String> trueClientInputsToTest;
    List<String> falseClientInputsToTest;
    boolean answer;

    public TrueFalse(int id,String question, Boolean clientAnswer) {
        super(id,question, clientAnswer.toString(), 1);
        trueClientInputsToTest = List.of("vrai", "true", "y");
        falseClientInputsToTest = List.of("faux", "false", "n");
        answer = clientAnswer;
    }

    public boolean checkAnswer(String answerToConvert) {
       if(checkMultipleInputs(answerToConvert) != null)
           return checkMultipleInputs(answerToConvert) == answer;
       else
           return false;
    }

    public Boolean checkMultipleInputs(String answerToConvert) {
        if (trueClientInputsToTest.contains(answerToConvert.toLowerCase())) {
            return true;
        }
        else if (falseClientInputsToTest.contains(answerToConvert.toLowerCase())) {
            return  false;
        }
        else{
            return null;
        }
    }
}


