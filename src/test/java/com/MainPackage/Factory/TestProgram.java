package com.MainPackage.Factory;


import QuestionTypes.QCM;
import QuestionTypes.Question;
import com.MainPackage.Service.IdGenerator;
import com.MainPackage.UserObject;
import com.zenika.academy.tmdb.MovieInfo;
import com.zenika.academy.tmdb.TmdbClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.time.Year;
import java.util.List;
import java.util.Optional;


public class TestProgram {
    //TODO GERER L'ID DES TESTS et l'instanciation du IDGenerator
    @Test
    public void testCheckAnswer() {
        QCM qcm = new QCM(1,"capitale de la france ?", "paris", List.of("jambon", "Tourbilol", "gloupi"));
        Assertions.assertTrue(qcm.checkAnswer("paris"));
    }

    @Test
    public void testCheckAnswerWithLevenshteinDistance() {
        Question question = new Question(1,"capitale de la france ?", "paris", 2);
        Assertions.assertTrue(question.checkAnswer("poris"));
    }

    @Test
    public void testIncreaseUserScore() {
        UserObject userObject = new UserObject();
        userObject.increaseUserScore(8);
        Assertions.assertEquals(8, userObject.getUserScore());
    }

    @Test
    public void testGetNewQuestionFromTmdbApi() throws IOException {

        TmdbClient mockTmdbClient = Mockito.mock(TmdbClient.class);

        Mockito.when(mockTmdbClient.getMovie("Titanic")).
                thenReturn(Optional.of(new MovieInfo("Titanic", Year.of(1997), List.of(
                        new MovieInfo.Character("", "Kate Winslet"),
                        new MovieInfo.Character("", "Leonardo DiCaprio"),
                        new MovieInfo.Character("", "Billy Zane")))));

        CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi = new CreateNewQuestionFromTmdbApi(new IdGenerator());
        Assertions.assertEquals("Quel film sorti en 1997 contient des personnages joués par les acteurs suivants :" +
                        "  - Kate Winslet - Leonardo DiCaprio - Billy Zane",
                createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi("Titanic", 0,mockTmdbClient).get().getQuestionText());
        Assertions.assertEquals("Titanic",
                createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi("Titanic", 0,mockTmdbClient).get().getAnswerText());
    }

    @Test
    public void testGetNewQuestionFromTmdbApiWithoutMatch() throws IOException {

        TmdbClient mockTmdbClient = Mockito.mock(TmdbClient.class);
        Mockito.when(mockTmdbClient.getMovie("TitleTest")).thenReturn(Optional.empty());

        CreateNewQuestionFromTmdbApi createNewQuestionFromTmdbApi = new CreateNewQuestionFromTmdbApi(new IdGenerator());
        Assertions.assertFalse(createNewQuestionFromTmdbApi.GetNewQuestionFromTmdbApi("TitleTest", 0,mockTmdbClient).isPresent());
    }

}
